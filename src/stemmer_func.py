# -*- coding: utf-8 -*-
#!/usr/bin/python

#	stemmer_gr.py is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    stemmer_gr.py is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

#############################################################################################################
#																											#
# 	AUTHOR	: Konstantinos Pechlivanis & Eirini Florou 					        							#
#																											#
#	LAST MOD: 14/01/2015																					#
#																											#
#	SUMMARY : this funtion finds the lemma of words (except for verbs), function "stem" estimates the lemma #
#			  and function "ends_with" estimates the suffix													#
#																											#
#	OUTPUS	: return the lemma of word																		#
#																					  						#
#############################################################################################################

# maintain word as initial
non_cases = ['ΑΛΦΑ', 'ΒΗΤΑ', 'ΓΑΜΜΑ', 'ΔΕΛΤΑ', 'ΕΨΙΛΟΝ', 'ΗΤΑ', 'ΘΗΤΑ', 'ΙΩΤΑ', 'ΚΑΠΠΑ', 'ΛΑΜΔΑ', 'ΜΙ', 'ΝΙ', 'ΞΙ', 'ΟΜΙΚΡΟΝ', 'ΤΟΙ', 'ΟΥΙΣΚΙ',
             'ΡΟ', 'ΣΙΓΜΑ', 'ΤΑΥ', 'ΥΨΙΛΟΝ', 'ΦΙ', 'ΧΙ', 'ΨΙ', 'ΩΜΕΓΑ', 'ΜΑΝΑΤΖΕΡ', 'ΝΤΕΝΤΕΚΤΙΒ', 'ΝΤΕΤΕΚΤΙΒ', 'ΠΛΑΖ', 'ΤΖΙΝ', 'ΡΟΜΑ', 'ΑΡΓΚΟ',
             'ΠΑΡΜΠΡΙΖ', 'ΠΑΡΤΙ', 'ΠΑΡΤΥ', 'ΡΑΝΤΕΒΟΥ', 'ΡΕΠΟ', 'ΤΑΝΚΣ', 'ΤΡΑΚ', 'ΦΕΡΜΟΥΡΑΡ', 'ΣΤΡΕΣ', 'ΜΑΤΣ', 'ΚΥΡ', 'ΜΠΛΕ', 'ΑΣ', 'ΕΝ', 'ΜΩΒ',
             'ΚΟΝΦΕΡΑΣΙΕ', 'ΜΕΤΡ', 'ΝΤΕΚΟΡΑΤΕΡ', 'ΚΕΙΚ', 'ΜΟΥΣ', 'ΚΟΜΠΙΟΥΤΕΡΣ', 'ΚΟΝΙΑΚ', 'ΒΙΝΤΑΤΖ', 'ΣΜΑΡΤΦΟΝ', 'ΡΙΓΕ', 'ΧΕΡΟΥΒΕΙΜ', 'ΓΙΟΓΚΑ', 
             'ΒΟΥΛΚΑΝΙΖΑΤΕΡ', 'ΑΣΑΝΣΕΡ', 'ΜΠΑΡ', 'ΚΛΑΜΠ', 'ΜΕΝΟΥ', 'ΤΙΡΜΠΟΥΣΟΝ', 'ΜΠΑΣΚΕΤ', 'ΧΟΤ-ΝΤΟΓΚ', 'ΤΟΤΕΜ', 'ΚΑΠΟΥΤΣΙΝΟ', 'ΛΟΥΞ', 'ΚΑΦΕ',
             'ΠΙΡΣΙΝΓΚ', 'ΜΑΝΙΚΙΟΥΡ', 'ΣΤΡΑΣ', 'ΤΗΛΕΚΟΝΤΡΟΛ', 'ΠΙΛΑΤΕΣ', 'ΚΡΟΣΦΙΤ', 'ΑΓΚΑΖΕ', 'ΓΚΡΙ', 'ΚΟΜΠΛΕ', 'ΤΗΣ', 'ΠΑΣΧΑ', 'ΠΕΡΣΙ', 'ΣΕΦ',
             'ΕΝΝΕΑ', 'ΣΙΚ', 'ΦΙΣΚΑ', 'ΠΡΟΦΙΤΕΡΟΛ', 'ΜΙΛΦΕΙΓ', 'ΚΕΛΣΙΟΥ', 'ΔΥΟ', 'ΠΕΝΤΕ', 'ΕΞΙ', 'ΕΠΤΑ', 'ΕΦΤΑ', 'ΤΑΙΜΣ', 'ΧΙΠΣΤΕΡ', 'ΔΕΚΑΕΞΙ', 
             'ΟΚΤΩ', 'ΟΧΤΩ', 'ΕΝΝΙΑ', 'ΔΕΚΑ', 'ΕΝΔΕΚΑ', 'ΕΝΤΕΚΑ', 'ΔΩΔΕΚΑ', 'ΔΕΚΑΠΕΝΤΕ', 'ΔΕΚΑΞΙ', 'ΔΕΚΑΕΠΤΑ', 'ΔΕΚΑΕΦΤΑ', 'ΣΧΕΔΟΝ', 'ΕΜΠΡΙΜΕ',
             'ΔΕΚΑΟΚΤΩ', 'ΔΕΚΑΟΧΤΩ', 'ΔΕΚΑΕΝΝΕΑ', 'ΔΕΚΑΕΝΝΙΑ', 'ΕΙΚΟΣΙ', 'ΕΚΑΤΟΝ', 'ΚΑΤΩ', 'ΠΑΝΩ', 'ΠΕΡΑ', 'ΔΩΘΕ', 'ΤΑΝΓΚΟ', 'ΒΑΒΕΛ', 'ΜΟΝΤΕΜ', 
             'ΜΟΛΙΣ', 'ΕΞΩ', 'ΕΣΩ', 'ΜΕΣΑ', 'ΚΙΟΛΑΣ', 'ΗΔΗ', 'ΠΙΑ', 'ΠΑΡΑ', 'ΜΕΤΑ', 'ΓΙΑΤΙ', 'ΔΗΘΕΝ', 'ΣΑ', 'ΤΑΧΑ', 'ΙΝΤΕΡΝΕΤ', 'ΠΑΝΟ', 'ΕΙΘΕ',
             'ΦΕΤΟΣ', 'ΕΦΕΤΟΣ', 'ΕΠΑΝΩ', 'ΠΡΩΗΝ', 'ΕΞΗΣ', 'ΕΔΩ', 'ΕΚΕΙ', 'ΕΝΑΝΤΙ', 'ΤΕΩΣ', 'ΝΥΝ', 'ΩΣΤΟΣΟ', 'ΕΤΣΙ', 'ΙΣΩΣ', 'ΛΟΙΠΟΝ', 'ΜΑΛΛΟΝ',
             'ΝΑΙ', 'ΟΧΙ', 'ΠΡΑΓΜΑΤΙ', 'ΑΝΤΕ', 'ΑΙΝΤΕ', 'ΑΛΙΜΟΝΟ', 'ΑΛΟΙΜΟΝΟ', 'ΕΜΠΡΟΣ', 'ΜΠΡΟΣ', 'ΜΠΡΟΣΤΑ', 'ΠΙΣΩ', 'ΠΛΑΙ', 'ΚΡΙΜΑ', 'ΜΑΚΑΡΙ',
             'ΕΥΓΕ', 'ΜΠΡΑΒΟ', 'ΚΑΤΟΠΙΝ', 'ΜΕΤΕΠΕΙΤΑ', 'ΥΣΤΕΡΑ', 'ΑΝΑΜΕΣΑ', 'ΓΥΡΩ', 'ΔΙΠΛΑ', 'ΑΜΠΟΤΕ', 'ΠΙΘΑΝΟΝ', 'ΠΙΟ', 'ΦΑΞ', 'ΣΚΕΙΤ', 'ΤΙΜ', 
             'ΠΑΝΤΟΥ', 'ΠΟΥΘΕΝΑ', 'ΟΠΟΥΔΗΠΟΤΕ', 'ΟΠΩΣΔΗΠΟΤΕ', 'ΠΩΣ', 'ΠΟΤΕ', 'ΠΟΥ', 'ΑΝΤΙΚΡΥ', 'ΚΑΤΑΜΕΣΙΣ', 'ΑΚΟΜΗ', 'ΑΚΟΜΑ', 'ΕΠΕΙΤΑ', 'ΤΡΙΚ',
             'ΜΕΘΑΥΡΙΟ', 'ΝΩΡΙΣ', 'ΞΑΝΑ', 'ΠΑΛΙ', 'ΧΤΕΣ', 'ΠΡΟΧΤΕΣ', 'ΣΗΜΕΡΑ', 'ΕΝΤΕΛΩΝ', 'ΕΞΙΣΟΥ', 'ΟΛΟΤΕΛΑ', 'ΟΛΩΣΣΔΙΟΛΟΥ', 'ΚΑΠΟΥ', 'ΠΑΝΤΑ',
             'ΚΑΠΩΣ', 'ΑΛΛΟΥ', 'ΑΛΛΟΤΕ', 'ΑΛΛΙΩΣ', 'ΤΩΡΑ', 'ΤΟΤΕ', 'ΟΠΟΥ', 'ΟΠΟΤΕ', 'ΟΠΩΣ', 'ΟΠΟΤΕΔΗΠΟΤΕ', 'ΑΝ', 'ΝΑ', 'ΜΗ', 'ΔΕΝ', 'ΠΟΡΤΑΤΙΦ',
             'ΠΑΝΤΟΤΕ', 'ΚΑΘΟΛΟΥ', 'ΑΛΛΩΣΤΕ', 'ΔΗΛΑΔΗ', 'ΕΞΑΛΛΟΥ', 'ΟΜΩΣ', 'ΑΡΑΓΕ', 'ΜΑΛΙΣΤΑ', 'ΜΗΠΩΣ', 'ΤΩΟΝΤΙ', 'ΣΕ', 'ΓΙΑ', 'ΒΙΝΤΕΟ', 'ΟΦΗ',
             'ΑΝΑ', 'ΑΝΤΙ', 'ΕΚΤΟΣ', 'ΕΝΑΝΤΙΟΝ', 'ΕΝΩΠΙΟΝ', 'ΕΞΑΙΤΙΑΣ', 'ΕΩΣ', 'ΙΣΑΜΕ', 'ΚΑΤΑ', 'ΜΕΣΩ', 'ΜΕΤΑΞΥ', 'ΣΠΟΤ', 'ΚΑΠΟΤΕ', 'ΑΠΕΝΑΝΤΙ',
             'ΜΕΧΡΙ', 'ΠΛΗΝ', 'ΠΡΙΝ', 'ΩΣΑΝ', 'ΧΩΡΙΣ', 'ΩΣΠΟΥ', 'ΕΝΕΚΑ', 'ΠΕΡΙ', 'ΠΡΟ', 'ΔΙΑ', 'ΣΥΝ', 'ΑΝΕΥ', 'ΕΠΙ', 'ΤΩΝ', 'ΚΡΑΓΙΟΝ', 'ΣΟΦΕΡ',
             'ΥΠΟ', 'ΕΙΣ', 'ΕΝΕΚΕΝ', 'ΖΕΝΙΘ', 'ΝΑΔΙΡ', 'ΦΑΟΥΛ', 'ΚΕΜΠΑΠ', 'ΣΙΝΕΜΑ', 'ΜΟΤΕΡ', 'ΝΕΣΕΣΕΡ', 'ΕΝΤΟΣ', 'ΚΑΛΟΡΙΦΕΡ', 'ΡΕΦΡΕΝ', 'ΠΑΟΚ',
             'ΜΑΝΤΑΜ', 'ΚΡΟΥΑΣΑΝ', 'ΜΕΙΚΑΠ', 'ΑΜΠΑΖΟΥΡ', 'ΜΑΣΚΟΤ', 'ΝΤΙΣΚΟ', 'ΧΑΚΙ', 'ΠΛΙΑΤΣΙΚΟ', 'ΟΡΝΤΕΒ', 'ΜΠΟΥΝΤΟΥΑΡ', 'ΛΙΦΤΙΝΓΚ', 'ΤΑΓΙΕΡ',
             'ΤΡΟΙΚΑ', 'ΤΣΑΙ', 'ΑΝΤΕΝΑ', 'ΧΑΣΤΑΓΚ', 'ΤΟΥΙΤΕΡ', 'ΦΟΛΟΟΥ', 'ΚΟΥΛ', 'ΣΟΡΙ', 'ΓΚΛΙΤΕΡ', 'ΝΤΙΖΑΙΝ', 'ΜΠΡΟΟΥΖΕΡ', 'ΟΦ', 'ΑΠΟ', 'ΧΟΛ',
             'ΠΑΠΙΓΙΟΝ', 'ΕΥΡΩ', 'ΓΙΟΥΡΟ', 'ΓΚΡΟΥΠ', 'ΠΙ', 'ΚΑΝΤΡΑΝ', 'ΝΤΕΚΟΥΠΑΖ', 'ΚΡΑΚΕΛΕ', 'ΦΕΤΙΧ', 'ΝΑΖΙ', 'ΜΕ', 'ΤΕΝΙΣ', 'ΡΟΜΠΟΤ', 'ΚΑΡΟ',
             'ΦΙΝΑΛΕ', 'ΜΟΤΟ', 'ΤΡΜΠΛ', 'ΜΠΑΡΑΖ', 'ΤΣΑΜΠΙΟΝΣ', 'ΛΙΓΚ', 'ΦΙΦΑ', 'ΟΥΕΦΑ', 'ΠΑΝΤΟΦΛΕ', 'ΣΕΞ', 'ΣΙΣΠΑΝΣΙΟΝ', 'ΦΟΥΛΑΡΙ', 'ΣΑΜΠΟΤΕΡ', 
			 'ΣΟΥΠΕΡ', 'ΑΕΚ', 'ΧΙΓΚΣ', 'ΝΟΜΠΕΛ', 'ΝΤΟΚΙΜΑΝΤΕΡ', 'ΝΤΟΚΥΜΑΝΤΕΡ', 'ΟΛΟΝΥΧΤΙΣ', 'ΣΑΝ', 'ΕΑΜ', 'ΕΠΟΝ', 'ΤΟΥΣ', 'ΜΑΚΙΓΙΑΖ', 'ΧΑΡΑΜΙ',
			 'ΕΛΑΣ', 'ΣΥΡΙΖΑ', 'ΚΚΕ', 'ΣΥΝΑΜΑ', 'ΔΙΧΩΣ', 'ΚΙΛΕΛΕΡ', 'ΛΟΓΩ', 'ΕΣΠΑ', 'ΜΜΕ', 'ΠΡΟΣ', 'ΠΛΕΟΝ', 'ΑΡΑ', 'ΜΕΣ', 'ΜΟΥ', 'ΒΙΖΟΝ', 'ΤΙ',
			 'ΜΠΑΖΑΡ', 'ΑΛΛΑΛΟΥΜ', 'ΚΑΛΑΖΝΙΚΟΦ', 'ΓΚΕΤΟ', 'ΚΟΜΕΝΤΙ', 'ΛΙΜΠΙΝΤΟ', 'ΠΟΡΝΟ', 'ΜΟΛΟΤΟΦ', 'ΑΠΕΥΘΕΙΑΣ', 'ΜΑΖΟΥΡΚΑ', 'ΚΟΛΑΝ', 'ΜΑΓΙΟ',
			 'ΤΡΑΚΤΕΡ', 'ΓΚΛΟΠ', 'ΚΑΡΑΜΠΙΝΙΕΡΙ', 'ΦΑΡΣΕΡ', 'ΠΑΝΕΛ', 'ΚΟΝΤΕΙΝΕΡ', 'ΝΕΟΝΑΖΙ', 'ΚΟΡΣΕ', 'ΑΛΤΣΧΑΙΜΕΡ', 'ΠΑΡΑΔΙΠΛΑ', 'ΣΕΡΙ', 'ΠΙΚΕ', 
			 'ΚΑΤΑΦΩΡΑ', 'ΑΒΛΕΠΙ', 'ΞΟΠΙΣΩ', 'ΟΛΟΠΛΕΥΡΑ', 'ΝΕΩΣΤΙ', 'ΑΝΤΙΚΚΕ', 'ΚΑΘΗΚΟΝ', 'ΜΕΛΛΟΝ', 'ΠΑΡΕΛΘΟΝ', 'ΠΑΡΟΝ', 'ΠΕΡΙΒΑΛΛΟΝ', 'ΒΟΛΕΙ',
			 'ΠΡΟΣΟΝ','ΣΥΜΒΑΝ', 'ΣΥΜΠΑΝ', 'ΦΩΝΗΕΝ', 'ΟΝ', 'ΜΠΟΝΟΥΣ', 'ΡΟΜΑΝΤΣΟ', 'ΡΟΖΕ', 'ΧΟΚΕΙ', 'ΠΡΟΤΖΕΚΤ', 'ΚΟΤΖΑΜ', 'ΜΙΤΙΓΚ', 'ΣΠΕΣΙΑΛΙΤΕ',
			 'ΧΟΥΙ', 'ΣΑΒΑΓΙΑΡ', 'ΣΑΒΑΡΕΝ', 'ΣΑΒΟΥΑΡ', 'ΒΙΒΡ', 'ΣΑΓΚΟΥΙΝΙ', 'ΓΚΟΛ', 'ΒΟΥΑΓΙΑΖ', 'ΣΑΜΟΥΑ', 'ΣΑΜΠΑΝΙΖΕ', 'ΣΑΜΠΟ', 'ΩΣ', 'ΠΕΡΥΣΙ',
			 'ΣΑΜΠΟΥΑΝ', 'ΣΑΝΤΙΓΙ', 'ΣΑΝΤΟΥΚ', 'ΣΑΝΤΡΕ', 'ΣΑΤΙΝΕ', 'ΣΕΒΡΟ', 'ΣΕΖΛΟΝΓΚ', 'ΣΕΖΟΝ', 'ΣΕΚΑΝΣ', 'ΣΕΝΙΑΝ', 'ΣΕΠΑΡΕ', 'ΒΟΥΑΛ', 'ΑΟΥΤ',
			 'ΣΕΡΖ', 'ΣΙΛΟ', 'ΣΕΣΟΥΑΡ', 'ΣΙΕΛ', 'ΣΙΚΛΑΜΕΝ', 'ΣΙΜΟΥΝ', 'ΣΙΡΚΟΥΙ', 'ΣΤΙΛ', 'ΣΤΡΙΠΤΙΖΕΖ', 'ΣΤΡΙΠΤΙΖ', 'ΤΑΚΤ', 'ΣΟΥΤ', 'ΠΑΝ', 'ΕΚ',
			 'ΤΑΛΚ', 'ΣΚΑΜΠΟ', 'ΣΟΜΙΕ', 'ΣΟΜΟΝ', 'ΣΟΤΕ', 'ΣΟΥΑΡΕ', 'ΣΟΥΒΕΝΙΡ', 'ΣΟΥΕΤ', 'ΣΟΦΙΣΤΙΚΕ', 'ΚΟΥΛΤΟΥΡΕ', 'ΛΑΙΚ', 'ΣΑΙΤ', 'ΚΟΛ', 'ΜΟΒ',
			 'ΤΑΜΠΑ', 'ΤΕΛΕΣΙΕΖ', 'ΤΙΡΑΖ', 'ΤΟΝΑΖ', 'ΤΟΥΓΙΑ', 'ΑΝΣΑΜΠΛ', 'ΣΑΡΠΑ', 'ΚΑΛΣΟΝ', 'ΚΑΜΠΑΡΝΤΙΝΑ', 'ΚΑΣΚΟΛ', 'ΤΑΦΤΑΣ', 'ΒΙΣΚΟΖ', 'ΡΟΖ',
			 'ΤΟΥΛΙ', 'ΣΟΛΦΕΖ', 'ΤΟΥΡΝΙΚΕ', 'ΤΡΑΒΕΣΤΙ', 'ΤΡΑΝΣ', 'ΤΡΑΒΕΛΙ', 'ΤΣΙΝΤΣΙΛΑ', 'ΑΜΠΙΓΙΕ', 'ΖΑΡΝΤΙΝΙΕΡΑ', 'ΜΠΟΡΝΤΟ', 'ΠΑΣΤΕΛ', 'ΠΑΛΤΟ',
			 'ΚΡΟΥΑΖΕ', 'ΜΑΝΕΚΕΝ', 'ΜΠΟΥΤΙΚ', 'ΝΤΕΜΟΝΤΕ', 'ΠΑΣΑΡΕΛΑ', 'ΡΕΒΕΡ', 'ΤΡΟΥΑΚΑΡ', 'ΦΕΡΜΟΥΑΡ', 'ΚΟΛΙΕ', 'ΜΕΝΤΑΓΙΟΝ', 'ΦΟΜΠΙΖΟΥ', 'ΑΚΑΖΟΥ',
			 'ΛΑΜΠΑΤΕΡ', 'ΜΠΙΜΠΕΛΟ', 'ΝΤΕΚΟΡ', 'ΑΜΠΙΓΙΕΡ', 'ΓΚΡΟ', 'ΠΛΑΝ', 'ΜΙΞΑΖ', 'ΜΟΝΤΑΖ', 'ΠΛΑΤΟ', 'ΦΕΣΤΙΒΑΛ', 'ΦΙΛΜ', 'ΝΟΥΑΡ', 'ΝΤΙΝΤΕΡΟ',
			 'ΤΥΡΚΟΥΑΖ', 'ΓΚΑΖΟΝ', 'ΚΑΝΑΠΕΣ', 'ΚΟΝΣΟΛΑ', 'ΡΕΤΙΡΕ', 'ΣΑΛΟΝΙ', 'ΣΕΚΡΕΤΕΡ', 'ΣΟΥΙΤΑ', 'ΦΕΡΦΟΡΖΕ', 'ΦΙΣ', 'ΝΤΟΥΜΠΛΕΦΑΣ', 'ΣΑΤΕΝ',
			 'ΑΠΕΡΙΤΙΦ', 'ΓΚΑΡΣΟΝ', 'ΕΚΛΕΡ', 'ΕΒΑΠΟΡΕ', 'ΜΠΕΣΑΜΕΛ', 'ΝΟΥΑ', 'ΡΕΣΤΟΡΑΝ', 'ΦΑΡΙΝ', 'ΛΑΚΤΕ', 'ΦΡΑΠΕ', 'ΦΡΙΚΑΣΕ', 'ΦΡΟΥΙ', 'ΜΑΣΑΛΛΑ',
			 'ΓΛΑΣΕ', 'ΜΠΟΥΑΤ', 'ΣΟΥΞΕ', 'ΤΟΥΡΝΕ', 'ΦΟΥΑΓΙΕ', 'ΚΟΜΠΡΕΣΕΡ', 'ΚΟΝΤΕΡ', 'ΡΟΤΑΣΙΟΝ', 'ΚΟΝΤΡΟΛ', 'ΝΤΟΥΙ', 'ΤΑΜΠΛΟ', 'ΚΑΠΟ', 'ΜΠΟΥΦΑΝ',
			 'ΑΞΕΣΟΥΑΡ', 'ΒΟΛΑΝ', 'ΜΠΟΥΖΙ', 'ΣΚΟΡΕΡ', 'ΧΙΟΥΜΟΡ', 'ΡΕΚΟΡ', 'ΤΕΡΕΝ', 'ΡΕΒΑΝΣ', 'ΚΟΥΛΟΥΑΡ', 'ΓΚΡΑΝΠΡΙ', 'ΛΟΥΤΡ', 'ΡΕΝΑΡ', 'ΕΜΠΑΡΓΚΟ',
			 'ΖΕΤΕ', 'ΑΡΑΣΕ', 'ΤΕΛΕΦΕΡΙΚ', 'ΒΑΓΚΟΝΛΙ', 'ΠΟΡΜΠΑΓΚΑΖ', 'ΡΕΖΕΡΒΟΥΑΡ', 'ΤΟΥΡΝΟΥΑ', 'ΑΡΝΟΥΒΟ', 'ΑΡΝΤΕΚΟ', 'ΑΤΕΛΙΕ', 'ΑΦΙΣΑ', 'ΤΡΟΤΣΚΥ',
			 'ΜΠΑΡΟΚ', 'ΝΤΟΚΤΟΡΑ', 'ΤΟΥΠΕ', 'ΡΕΣΙΤΑΛ', 'ΡΕΤΡΟ', 'ΚΡΟΟΥΛ', 'ΝΟΚ', 'ΠΟΛΟ', 'ΕΤΟΛ', 'ΣΑΚ', 'ΚΑΜΗΛΟ', 'ΜΠΡΟΚΑΡ', 'ΜΠΕΖ', 'ΜΑΡΕΝ',
			 'ΠΟΥΛΟΒΕΡ', 'ΤΖΑΚΕΤ', 'ΝΤΙΛΕΡ', 'ΡΕΠΟΡΤΕΡ', 'ΓΟΥΙΝΤΣΕΡΦΙΓΚ', 'ΝΤΕΡΜΠΙ', 'ΡΑΛΙ', 'ΡΙΓΚ', 'ΣΚΟΡ', 'ΣΠΟΡ', 'ΤΖΟΚΕΙ', 'ΕΡΜΠΑΣ', 'ΤΑΝΚΕΡ',
			 'ΤΖΑΜΠΟ', 'ΝΤΙΟΥΤΙ', 'ΦΡΙ', 'ΣΠΟΝΣΟΡΑΣ', 'ΦΕΡΙΜΠΟ ΓΚΡΕΙΠ', 'ΦΡΟΥΤ', 'ΓΡΙΛ', 'ΚΕΤΣΑΠ', 'ΚΟΚΤΕΙΛ', 'ΓΙΟΥΝΙΣΕΞ', 'ΛΙΛΑ', 'ΝΤΡΕΥΦΟΥΣ',
			 'ΚΟΡΝ', 'ΦΛΕΙΚΣ', 'ΚΡΑΚΕΡ', 'ΤΣΕΚ', 'ΜΠΑΡΜΠΕΚΙΟΥ', 'ΜΠΕΙΚΟΝ', 'ΣΑΝΤΟΥΙΤΣ', 'ΧΙΟΝΕ', 'ΠΑΝΚ', 'ΓΚΕΙ', 'ΧΑΜΠΟΥΡΓΚΕΡ', 'ΕΡ', 'ΚΟΝΤΙΣΙΟΝ',
			 'ΚΑΜΕΡΑ', 'ΚΛΑΞΟΝ', 'ΚΟΠΙΡΑΙΤ', 'ΜΟΝΙΤΟΡ', 'ΜΠΑΙΠΑΣ', 'ΜΠΛΑΚ', 'ΟΥΤ', 'ΡΑΝΤΑΡ', 'ΣΕΛΦ', 'ΤΣΙΠΣ', 'ΤΣΙΛΙ', 'ΣΕΛΦΙ', 'ΤΟΝΕΡ', 'ΑΝΤΙΣΟΝ',
			 'ΝΤΡΕΣΙΓΚ', 'ΠΙΛΙΓΚ', 'ΣΕΡΒΙΣ', 'ΣΛΑΙΤΣ', 'ΤΖΙΠ', 'ΤΟΥΝΕΛ', 'ΤΣΕΚΑΠ', 'ΛΙΠΓΚΛΟΣ', 'ΜΑΞΙ', 'ΜΙΝΙ', 'ΝΑΙΛΟΝ', 'ΣΟΡΤ', 'ΑΛΜΠΟΥΜ', 'ΠΕΗΤΖ',
			 'ΓΚΕΡΛ', 'ΑΝΤΕΡΓΚΡΑΟΥΝΤ', 'ΑΟΥΤΣΑΙΝΤΕΡ', 'ΒΙΝΤΕΟΚΛΙΠ', 'ΓΚΕΣΤ', 'ΣΤΑΡ', 'ΓΟΥΕΣΤΕΡΝ', 'ΓΚΑΡΝΤΕΝ', 'ΚΑΣΤ', 'ΚΛΟΟΥΝ', 'ΕΚΡΟΥ', 'ΚΡΕΜ',
			 'ΚΟΥΙΖ', 'ΜΙΟΥΖΙΚΑΛ', 'ΜΠΛΟΥΤΖΙΝ', 'ΜΠΟΟΥΛΙΓΚ', 'ΠΛΕΙ', 'ΣΙΤΕΡ', 'ΠΟΣΤΕΡ', 'ΣΙΡΙΑΛ', 'ΚΙΛΕΡ', 'ΣΚΕΤΣ', 'ΣΚΡΑΜΠΛ', 'ΣΤΕΡΕΟ', 'ΝΤΙΖΕΛ',
			 'ΤΖΟΥΚ', 'ΜΠΟΞ', 'ΧΑΠΕΝΙΓΚ', 'ΧΕΒΙ', 'ΜΕΤΑΛ', 'ΛΑΙΒ', 'ΜΙΟΥΖΙΚ', 'ΧΟΥΛΙΓΚΑΝ', 'ΤΙΝΕΙΤΖΕΡ', 'ΒΕΡΜΟΥΤ', 'ΣΝΙΤΣΕΛ', 'ΣΟΟΥ', 'ΚΑΜΟΥΦΛΑΖ',
			 'ΣΟΥ', 'ΜΑΖΟΥΤ', 'ΜΠΑΚ', 'ΜΑΜΟΥΘ', 'ΣΚΙΝΧΕΝΤ', 'ΠΡΟΙΟΝ', 'ΠΟΛΚΑ', 'ΦΑΡΑΩ', 'ΜΑΝΝΑ', 'ΧΑΒΡΑ', 'ΩΣΑΝΝΑ', 'ΒΕΡΕΣΕ', 'ΜΠΕΚΙΝ', 'ΜΠΡΟΥΚΛΙΝ',
			 'ΙΣΛΑΜ', 'ΚΑΝΤΙΟ', 'ΚΟΥΛΟΥΒΑΧΑΤΑ', 'ΓΙΟΥΧΑ', 'ΖΑΦΤΙ', 'ΚΟΥΤΟΥΡΟΥ', 'ΜΠΙΤ', 'ΝΤΑΛΑ', 'ΝΤΙΠ', 'ΝΤΟΓΡΟΥ', 'ΑΜΗΝ', 'ΣΤΡΩΜΑΤΕΞ', 'ΜΑΝΧΑΙΜ',
			 'ΣΙΧΤΙΡ', 'ΤΖΑΜΠΑ', 'ΤΟΥΡΛΟΥ', 'ΦΑΡΣΙ', 'ΑΜΠΙΓΙΕΖ', 'ΛΙΛΕΖ', 'ΝΤΙΖΕΖ', 'ΑΒΑΝΤΑΖ', 'ΑΜΠΑΛΑΖ', 'ΓΚΑΡΑΖ', 'ΚΟΛΑΖ', 'ΚΟΡΣΑΖ', 'ΟΠΕΡΑΤΕΡ',
			 'ΜΑΣΑΖ', 'ΜΠΟΙΚΟΤΑΖ', 'ΠΑΤΙΝΑΖ', 'ΡΕΠΟΡΤΑΖ', 'ΦΙΡΙ', 'ΣΑΜΠΟΤΑΖ', 'ΤΑΤΟΥΑΖ', 'ΖΟΓΚΛΕΡ', 'ΚΑΣΚΑΝΤΕΡ', 'ΜΑΚΙΓΙΕΡ', 'ΜΑΣΕΡ', 'ΚΑΜΕΡΑΜΑΝ',
			 'ΜΠΟΞΕΡ', 'ΣΚΙΕΡ', 'ΑΜΟΡΤΙΣΕΡ', 'ΒΕΝΤΙΛΑΤΕΡ', 'ΚΑΡΜΠΥΡΑΤΕΡ', 'ΦΛΟΤΕΡ', 'ΤΕΛΕΞ', 'ΓΚΙΣΕ', 'ΜΠΡΕΛΟΚ', 'ΓΚΡΕΝΑ', 'ΙΒΟΥΑΡ', 'ΤΣΩΡΤΣΙΛ',
	  	     'ΑΤΡΑΞΙΟΝ', 'ΒΕΡΣΙΟΝ', 'ΙΛΟΥΣΤΡΑΣΙΟΝ', 'ΙΜΙΤΑΣΙΟΝ', 'ΡΟΥΛΕΜΑΝ', 'ΣΑΣΜΑΝ', 'ΑΦΡΟΛΕΞ', 'ΛΑΣΤΕΞ', 'ΝΤΟΥΠΛΕΞ', 'ΣΠΟΡΤΕΞ', 'ΠΑΝΕ', 'ΛΑΜΕ',
	  	     'ΓΚΑΛΕΡΙ', 'ΚΑΡΟΣΕΡΙ', 'ΟΥΖΕΡΙ', 'ΜΠΟΥΑΖΕΡΙ', 'ΝΤΡΑΠΑΡΙ', 'ΠΑΤΙΣΕΡΙ', 'ΤΑΓΕΡ', 'ΠΥΡΕΞ', 'ΑΠΛΙΚΕ', 'ΜΕΡΣΕΡΙΖΕ', 'ΡΑΜΠΟΤΕ', 'ΚΑΡΑΜΕΛΕ',
	  	     'ΡΑΦΙΝΕ', 'ΣΙΝΙΕ', 'ΣΙΚΕ', 'ΦΟΥΜΕ', 'ΑΓΟΡΕ', 'ΚΟΤΛΕ', 'ΚΟΥΡΕΛΕ', 'ΚΥΡΙΛΕ', 'ΜΠΑΛΑΡΙΝΕ', 'ΜΠΑΝΑΝΕ', 'ΡΕΖΕΡΒΕ', 'ΓΚΑΓΚΣΤΕΡ', 'ΜΠΕΙΜΠΙ',
	  	     'ΜΠΑΤΛΕΡ', 'ΣΠΙΚΕΡ', 'ΤΙΝΕΙΖΕΡ', 'ΒΙΟΥΜΑΣΤΕΡ', 'ΘΡΙΛΕΡ', 'ΚΟΜΠΙΟΥΤΕΡ', 'ΤΟΣΤ', 'ΤΟΥ', 'ΚΟΡΝΕΡ', 'ΚΟΥΑΚΕΡ', 'ΜΠΕΣΤΣΕΛΕΡ', 'ΜΠΛΕΙΖΕΡ',
	  	     'ΜΠΛΕΝΤΕΡ', 'ΡΕΒΟΛΒΕΡ', 'ΣΕΙΚΕΡ', 'ΤΣΑΡΤΕΡ', 'ΓΟΥΙΝΤ', 'ΣΕΡΦΙΓΚ', 'ΜΑΡΚΕΤΙΓΚ', 'ΛΙΦΤΙΓΚ', 'ΝΤΑΝΣΙΓΚ', 'ΣΜΟΚΙΝ', 'ΠΑΡΚΙΓΚ', 'ΣΟΟΥΜΑΝ',
	  	     'ΠΑΟΥΝΤΕΡ', 'ΜΠΟΝΤΙ', 'ΜΠΙΛΝΤΙΓΚ', 'ΣΟΚΙΝ', 'ΤΖΟΚΙΓΚ', 'ΚΑΜΠΙΓΚ', 'ΓΚΡΕΥ', 'ΓΚΩΛ', 'ΓΙΒΡΑΛΤΑΡ', 'ΒΑΝ', 'ΓΚΟΓΚ', 'ΡΟΥΖΒΕΛΤ', 'ΦΙΝΕΕΣ',
	  	     'ΓΟΥΟΚΜΑΝ', 'ΜΠΑΡΜΑΝ', 'ΜΠΑΡΓΟΥΜΑΝ', 'ΜΠΙΖΝΕΣΜΑΝ', 'ΠΟΥΛΜΑΝ', 'ΣΟΥΠΕΡΜΑΝ', 'ΤΖΕΝΤΛΕΜΑΝ', 'ΤΑΜΠΛΕΤ', 'ΜΙΞΕΡ', 'ΝΕΥ', 'ΝΕΒΙΣ', 'ΙΡΑΝ',
			 'ΑΒΡΑΑΜ', 'ΑΔΑΜ', 'ΒΑΡΛΑΑΜ', 'ΒΕΝΙΑΜΙΝ', 'ΓΑΒΡΙΗΛ', 'ΓΙΟΒΑΝ', 'ΓΙΟΥΡΙ', 'ΔΑΝΙΗΛ', 'ΔΑΥΙΔ', 'ΕΜΜΑΝΟΥΗΛ', 'ΕΥΦΡΑΙΜ', 'ΕΦΡΑΙΜ', 'ΜΙΧΑΗΛ',
			 'ΖΑΚΧΙΗΛ', 'ΖΑΝΝΙΝΟ', 'ΙΑΚΩΒ', 'ΙΣΑΑΚ', 'ΙΩΒ', 'ΙΩΑΚΕΙΜ', 'ΙΩΣΗΦ', 'ΚΑΙΣΑΡ', 'ΚΑΜΟΥΗΛ', 'ΚΑΧΙ', 'ΜΠΕΤΟΒΕΝ', 'ΝΑΘΑΝΑΗΛ', 'ΤΟΝΥ', 'ΠΩΛ',
			 'ΝΑΟΥΜ', 'ΝΤΑΝΙ', 'ΟΥΡΙΗΛ', 'ΠΑΒΕΛ', 'ΙΣΡΑΗΛ', 'ΡΑΟΥΛ', 'ΡΑΦΑΗΛ', 'ΡΟΜΑΝ', 'ΣΑΜΟΥΗΛ', 'ΣΕΡΑΦΕΙΜ', 'ΒΗΘΛΕΕΜ', 'ΒΑΝΟΥΑΤΟΥ', 'ΜΑΝΧΑΤΤΑΝ',
			 'ΒΙΒΙΑΝ', 'ΕΒΕΛΙΝ', 'ΕΜΙΛΥ', 'ΕΣΘΗΡ', 'ΣΑΟΥΛ', 'ΚΡΙΣΤΥ', 'ΜΙΣΕΛ', 'ΜΠΕΤΥ', 'ΡΑΧΗΛ', 'ΡΟΖΥ', 'ΣΕΣΙΛ', 'ΣΙΣΣΥ', 'ΑΖΕΡΜΠΑΙΤΖΑΝ', 'ΤΙΜΟΡ',
			 'ΑΦΓΑΝΙΣΤΑΝ', 'ΒΙΕΤΝΑΜ', 'ΓΚΑΜΠΟΝ', 'ΓΚΟΥΑΜ', 'ΜΠΙΣΣΑΟΥ', 'ΕΛ', 'ΣΑΛΒΑΔΟΡ', 'ΖΙΜΠΑΜΠΟΥΕ', 'ΚΑΖΑΚΣΤΑΝ', 'ΚΑΜΕΡΟΥΝ', 'ΜΑΓΙΟΤ', 'ΜΠΛΟΥΜ',
			 'ΚΑΤΑΡ', 'ΚΙΡΙΜΠΑΤΙ', 'ΚΟΥΒΕΙΤ', 'ΚΟΥΡΑΣΑΟ', 'ΛΙΧΤΕΝΣΤΑΙΝ', 'ΜΑΛΑΟΥΙ', 'ΙΡΑΚ', 'ΜΙΑΝΜΑΡ', 'ΜΟΝΑΚΟ', 'ΜΠΑΝΓΚΛΑΝΤΕΣ', 'ΒΕΡΝ', 'ΑΙΦΕΛ',
			 'ΜΠΑΡΜΠΑΝΤΟΣ', 'ΜΠΑΧΡΕΙΝ', 'ΜΠΕΛΙΖ', 'ΜΠΕΝΙΝ', 'ΜΠΟΤΣΟΥΑΝΑ', 'ΜΠΟΥΡΚΙΝΑ', 'ΜΠΟΥΡΟΥΝΤΙ', 'ΜΠΡΟΥΝΕΙ', 'ΝΑΜΙΜΠΙΑ', 'ΝΑΟΥΡΟΥ', 'ΠΑΚΙΣΤΑΝ',
			 'ΜΑΡΣΑΛ', 'ΝΙΟΥΕ', 'ΣΟΥΔΑΝ', 'ΝΕΠΑΛ', 'ΟΥΖΜΠΕΚΙΣΤΑΝ', 'ΠΑΛΑΟΥ', 'ΠΑΠΟΥΑ', 'ΠΕΡΟΥ', 'ΣΑΜΟΑ', 'ΣΑΟ', 'ΤΟΜΕ', 'ΤΡΙΝΙΝΤΑΝΤ', 'ΣΑΤΩΜΠΡΙΑΝ',
			 'ΤΟΜΠΑΓΚΟ', 'ΠΡΙΝΣΙΠΕ', 'ΤΣΑΝΤ', 'ΦΙΤΖΙ', 'ΣΙΕΡΑ', 'ΛΕΟΝΕ', 'ΣΟΥΡΙΝΑΜ', 'ΣΡΙ', 'ΤΑΙΒΑΝ', 'ΤΑΤΖΙΚΙΣΤΑΝ', 'ΤΖΙΜΠΟΥΤΙ', 'ΟΜΑΝ', 'ΟΥΓΚΟ',
			 'ΤΟΥΒΑΛΟΥ', 'ΤΟΥΡΚΜΕΝΙΣΤΑΝ', 'ΣΕΞΠΙΡ', 'ΒΑΛΕΡΙ', 'ΜΥΣΣΕ', 'ΡΟΣΣΙΝΙ', 'ΣΙΛΛΕΡ', 'ΒΕΡΛΑΙΝ', 'ΧΑΙΔΕΝ', 'ΜΠΟΝΤΛΑΙΡ', 'ΓΚΟΤΙΕ', 'ΜΟΠΑΣΣΑΝ',
			 'ΡΟΥΣΣΟ', 'ΜΠΟΥΑΛΟ', 'ΚΕΜΠΡΙΤΣ', 'ΜΟΝΤΡΕ', 'ΓΚΑΙΤΕ', 'ΓΚΑΙΜΠΕΛΣ', 'ΓΑΙΓΚΕΡ', 'ΧΑΙΝΤΕΛ', 'ΓΚΕΜΠΕΛΣ', 'ΧΕΝΤΕΛ', 'ΜΠΗΛΕΦΕΛΝΤ', 'ΣΛΗΜΑΝ',
			 'ΝΤΗΦΟΟΥ', 'ΓΚΡΗΝΟΥΙΤΣ', 'ΝΤΗΤΡΟΙΤ', 'ΣΑΙΞΠΗΡ', 'ΝΗΛ', 'ΝΤΗΖΕΛ', 'ΝΤΗΤΡΙΧ', 'ΛΗΜΠΕΡΜΑΝ', 'ΠΡΗΣΛΕΥ', 'ΡΕΜΠΩ', 'ΦΟΥΚΩ', 'ΦΩΚΝΕΡ', 'ΜΩΜ',
			 'ΩΣΤΕΝ', 'ΜΩΠΑΣΣΑΝ', 'ΓΚΩΤΙΕ', 'ΖΙΡΩΝΤΟΥ', 'ΝΤΕ', 'ΓΚΩΛΛ', 'ΤΡΥΦΦΩ', 'ΡΟΥΣΣΩ', 'ΓΚΕΤΕ', 'ΓΕΓΚΕΡ', 'ΣΩ', 'ΓΚΟΜΠΙΝΩ', 'ΚΛΕΜΑΝΣΩ', 'ΠΕΥ',
             'ΜΠΛΕΗΚ', 'ΝΤΡΕΗΚ', 'ΤΖΕΗΜΣ', 'ΧΕΜΙΝΓΚΓΟΥΕΗ', 'ΦΑΡΑΝΤΕΥ', 'ΒΑΤΕΡΛΩ', 'ΜΠΟΡΝΤΩ', 'ΜΥΝΣΤΕΡ', 'ΣΙΔΝΕΥ', 'ΧΟΛΛΥΓΟΥΝΤ', 'ΟΥΑΣΙΓΚΤΟΝ',
             'ΜΟΛΑΤΑΥΤΑ', 'ΣΑΝΓΚΑΗ', 'ΜΑΛΑΡΜΕ', 'ΦΡΑΝΚΕΝΣΤΑΙΝ', 'ΑΙΖΕΝΧΑΟΥΕΡ', 'ΑΝΤΕΝΑΟΥΕΡ', 'ΚΟΡΝΤΕΛ', 'ΧΑΛ', 'ΤΟΜΑΣ', 'ΜΑΝ', 'ΛΟΥΝΤ', 'ΜΑΝΧΑΤΤΑΝ',
             'ΑΝΤΙΣΟΝ', 'ΕΚΕΡΜΑΝ', 'ΜΠΛΑΚΟΥΕΛ', 'ΛΑΣΕΛΣ', 'ΑΣΑΦΦΕΜΠΟΥΡΓΚ', 'ΕΣΣΟΛΤΖ', 'ΟΥΩΛΤΕΡ', 'ΣΚΟΤ', 'ΜΠΟΣΣΥΕ', 'ΣΑΤΩΜΠΡΙΑΝ']

# just remove Σ and take the lemma
extra_s = ['ΚΡΕΑΣ', 'ΓΗΡΑΣ', 'ΠΕΡΑΣ', 'ΤΕΡΑΣ', 'ΣΕΒΑΣ', 'ΦΩΣ', 'ΗΜΙΦΩΣ', 'ΑΕΡΙΟΦΩΣ', 'ΓΕΓΟΝΟΣ', 'ΚΑΘΕΣΤΩΣ', 'ΛΥΚΟΦΩΣ']

irregular_adj = ['ΑΝΩ', 'ΚΑΤΩ', 'ΕΞΩ', 'ΕΣΩ', 'ΑΠΩ', 'ΥΠΕΡ']
endon = ['ΕΝΔΟΤΕΡΟΣ', 'ΕΝΔΟΤΕΡΗ', 'ΕΝΔΟΤΕΡΟ', 'ΕΝΔΟΤΕΡΟΥ', 'ΕΝΔΟΤΕΡΗΣ', 'ΕΝΔΟΤΕΡΟΙ', 'ΕΝΔΟΤΕΡΕΣ', 'ΕΝΔΟΤΕΡΑ']
plision = ['ΠΛΗΣΙΕΣΤΕΡΟΣ', 'ΠΛΗΣΙΕΣΤΕΡΗ', 'ΠΛΗΣΙΕΣΤΕΡΟ', 'ΠΛΗΣΙΕΣΤΕΡΟΥ', 'ΠΛΗΣΙΕΣΤΕΡΗΣ', 'ΠΛΗΣΙΕΣΤΕΡΟΙ', 'ΠΛΗΣΙΕΣΤΕΡΕΣ', 'ΠΛΗΣΙΕΣΤΕΡΑ']
protimo = ['ΠΡΟΤΙΜΟΤΕΡΟΣ', 'ΠΡΟΤΙΜΟΤΕΡΗ', 'ΠΡΟΤΙΜΟΤΕΡΟ', 'ΠΡΟΤΙΜΟΤΕΡΟΥ', 'ΠΡΟΤΙΜΟΤΕΡΗΣ', 'ΠΡΟΤΙΜΟΤΕΡΟΙ', 'ΠΡΟΤΙΜΟΤΕΡΕΣ', 'ΠΡΟΤΙΜΟΤΕΡΑ']
numbers = ['ΔΥΟ', 'ΠΕΝΤΕ', 'ΕΞΙ', 'ΕΠΤΑ', 'ΕΦΤΑ', 'ΟΚΤΩ', 'ΟΧΤΩ', 'ΕΝΝΕΑ', 'ΕΝΝΙΑ', 'ΔΕΚΑ', 'ΕΝΔΕΚΑ', 'ΕΝΤΕΚΑ', 'ΔΩΔΕΚΑ', 'ΔΕΚΑΠΕΝΤΕ',
           'ΔΕΚΑΕΞΙ', 'ΔΕΚΑΞΙ', 'ΔΕΚΑΠΕΤΑ', 'ΔΕΚΑΕΦΤΑ', 'ΔΕΚΑΟΚΤΩ', 'ΔΕΚΑΟΧΤΩ', 'ΔΕΚΑΕΝΝΕΑ', 'ΔΕΚΑΕΝΝΙΑ', 'ΕΙΚΟΣΙ', 'ΤΡΙΑΝΤΑ', 'ΣΑΡΑΝΤΑ',
           'ΠΕΝΗΝΤΑ', 'ΕΞΗΝΤΑ', 'ΕΒΔΟΜΗΝΤΑ', 'ΟΓΔΟΝΤΑ', 'ΕΝΕΝΗΝΤΑ', 'ΕΚΑΤΟΝ', 'ΕΚΑΤΟ']

def ends_with(word, suffix):
    return word[len(word) - len(suffix):] == suffix

def stem(word, typePOS):

    #print 'Type of ' + word + '-->' + typePOS

######rule-set 1Α ---> non-cases
    if word in non_cases :
        return word

######rule-set 1Β ---> ΕΝΔΟΝ
    elif word in endon :
        return 'ΕΝΔΟΝ'

######rule-set 1Γ ---> ΠΛΗΣΙΟΝ
    elif word in plision :
        return 'ΠΛΗΣΙΟΝ'

######rule-set 1Δ ---> ΠΡΟΤΙΜΩ
    elif word in protimo :
        return 'ΠΡΟΤΙΜΩ'

######rule-set 1Δ ---> Numbers
    elif word in numbers :
        return word

######rule-set 2 ---> noun with extra s
    if word in extra_s :
        return  word.decode('utf-8')[:-1]

###### rule-set 3 ---> neuter noun with suffix ΜΑΤΟΣ, ΜΑΤΑ, ΜΑΤΩΝ, ΜΑ
	if typePOS in ['NNN', 'NNSN', 'NNPN', 'NNPSN'] :
		for suffix in ['ΜΑΤΟΣ', 'ΜΑΤΩΝ', 'ΜΑΤΑ', 'ΜΑ'] :
			if ends_with(word, suffix) :
				return word[:len(word) - len(suffix)] + 'Μ'

###### rule-set 4 ---> suffix for proper names
	if typePOS in ['NNPM', 'NNPF', 'NNPN', 'NNPSM', 'NNPSF', 'NNPSN'] :
		for suffix in ['ΟΝΟΣ', 'ΩΝΟΣ', 'ΟΡΟΣ', 'ΕΥΣ', 'ΕΩΣ', 'ΟΝΤΟΣ', 'ΚΤΟΣ', 'ΟΥΣ'] :
			if ends_with(word, suffix) :
				return word[:len(word) - len(suffix)]
		for suffix in ['ΩΝ', 'ΩΡ', 'ΙΣ', 'Ξ', 'Ω', 'ΩΣ'] :
			if ends_with(word, suffix) :
				return word[:len(word) - len(suffix)]

###### rule-set 5 ---> irregular adjective
    for cur_word in irregular_adj:		# for every word in irregular adjective
		suf = len(word)-len(cur_word) 	# number of additional letters-suffix
		if word[:-suf] == cur_word:
			for suffix in ['ΤΕΡΟΥΣ', 'ΤΕΡΟΣ', 'ΤΕΡΟΝ', 'ΤΕΡΟΥ', 'ΤΕΡΗΣ', 'ΤΕΡΟΙ', 'ΤΕΡΩΝ', 'ΤΕΡΕΣ', 'ΤΑΤΟΣ', 'ΤΑΤΟΥ', 'ΤΑΤΗΣ', 'ΤΑΤΟΙ', 'ΤΑΤΩΝ', 'ΤΑΤΕΣ']:
				if ends_with(word, suffix) :
					return word[:len(word) - len(suffix)]
			for suffix in ['ΤΕΡΟ', 'ΤΕΡΗ', 'ΤΕΡΑ', 'ΤΑΤΟ', 'ΤΑΤΗ', 'ΤΑΤΑ']:
				if ends_with(word, suffix) :
					return word[:len(word) - len(suffix)]

#######rule-set 6 ---> Adjectives AND Participles
    if ('JJ' in typePOS) or (typePOS == 'CD') or (typePOS in ['VBG', 'VBP', 'VBPD', 'PRP', 'PP', 'REP', 'DP', 'IP', 'WP', 'QP', 'INP']):
        med_suffix = ['ΜΕΝΟΣ', 'ΜΕΝΟΥ', 'ΜΕΝΗΣ', 'ΜΕΝΟΙ', 'ΜΕΝΩΝ', 'ΩΜΕΝΟ', 'ΩΜΕΝΗ', 'ΩΜΕΝΑ', 'ΥΤΕΡΑ', 'ΥΤΑΤΑ', 'ΥΤΕΡΟ', 'ΟΤΑΤΗ', 'ΜΕΝΕΣ',
                      'ΟΜΕΝΑ', 'ΩΜΕΝΟ', 'ΩΜΕΝΗ', 'ΟΤΕΡΟ', 'ΟΤΕΡΗ', 'ΕΙΣΕΣ', 'ΟΜΕΝΟ', 'ΟΜΕΝΗ', 'ΥΤΕΡΗ', 'ΟΤΕΡΑ', 'ΜΕΝΟΙ', 'ΥΤΑΤΗ', 'ΟΤΑΤΟ',
                      'ΟΤΑΤΑ', 'ΜΕΝΟΥ', 'ΜΕΝΟΣ', 'ΗΜΕΝΗ', 'ΜΕΝΩΝ', 'ΜΕΝΗΣ', 'ΗΜΕΝΟ', 'ΗΜΕΝΑ', 'ΟΝΤΑΣ', 'ΩΝΤΑΣ', 'ΩΤΕΡΟ', 'ΩΤΕΡΕ', 'ΩΤΕΡΗ',
                      'ΩΤΕΡΑ', 'ΩΤΑΤΟ', 'ΩΤΑΤΕ', 'ΩΤΑΤΗ', 'ΩΤΑΤΑ']
        mdbig_suffix = ['ΥΤΕΡΕΣ', 'ΩΜΕΝΟΥ', 'ΟΤΑΤΩΝ', 'ΕΣΤΑΤΟ', 'ΕΣΤΑΤΗ', 'ΥΤΑΤΩΝ', 'ΥΤΕΡΗΣ', 'ΟΜΕΝΟΣ', 'ΟΤΕΡΟΙ', 'ΟΤΕΡΩΝ', 'ΥΤΑΤΟΣ', 'ΥΤΑΤΟΥ',
                        'ΕΣΤΑΤΑ', 'ΥΤΑΤΗΣ', 'ΟΤΕΡΟΣ', 'ΟΤΕΡΟΥ', 'ΥΤΑΤΕΣ', 'ΟΤΕΡΕΣ', 'ΥΤΕΡΟΙ', 'ΥΤΕΡΩΝ', 'ΑΙΤΕΡΟ', 'ΟΤΕΡΗΣ', 'ΥΤΕΡΟΣ', 'ΑΙΤΕΡΗ',
                        'ΑΙΤΕΡΑ', 'ΜΕΝΟΥΣ', 'ΥΤΕΡΟΥ', 'ΩΜΕΝΗΣ', 'ΩΜΕΝΩΝ', 'ΩΜΕΝΕΣ', 'ΟΥΜΕΝΟ', 'ΟΥΜΕΝΗ', 'ΟΥΜΕΝΑ', 'ΟΜΕΝΕΣ', 'ΩΜΕΝΟΣ', 'ΟΜΕΝΗΣ',
                        'ΟΜΕΝΩΝ', 'ΕΣΤΕΡΟ', 'ΕΣΤΕΡΗ', 'ΕΣΤΕΡΑ', 'ΟΤΑΤΟΣ', 'ΟΤΑΤΗΣ', 'ΟΜΕΝΟΥ', 'ΟΤΑΤΟΙ', 'ΥΤΑΤΟΙ', 'ΟΤΑΤΟΥ', 'ΗΜΕΝΗΣ', 'ΟΜΕΝΟΙ',
                        'ΗΜΕΝΟΥ', 'ΗΜΕΝΟΙ', 'ΗΜΕΝΩΝ', 'ΜΕΝΟΥΣ', 'ΗΜΕΝΟΣ', 'ΩΜΕΝΟΙ', 'ΟΤΑΤΕΣ', 'ΩΤΕΡΟΣ', 'ΩΤΕΡΟΥ', 'ΩΤΕΡΟΝ', 'ΩΤΕΡΟΙ', 'ΩΤΕΡΩΝ',
                        'ΩΤΕΡΗΣ', 'ΩΤΕΡΕΣ', 'ΩΤΕΡΑΣ', 'ΩΤΑΤΟΣ', 'ΩΤΑΤΟΥ', 'ΩΤΑΤΟΙ', 'ΩΤΑΤΩΝ', 'ΩΤΑΤΗΣ', 'ΩΤΑΤΕΣ']
        big_suffix = [ 'ΥΤΕΡΟΥΣ', 'ΕΣΤΕΡΟΙ', 'ΕΣΤΕΡΩΝ', 'ΕΣΤΕΡΕΣ', 'ΟΥΣΤΕΡΗ', 'ΩΜΕΝΟΥΣ', 'ΕΣΤΑΤΗΣ', 'ΕΣΤΕΡΑΣ', 'ΕΣΤΕΡΗΣ', 'ΟΥΣΤΕΡΟ', 'ΑΣΜΕΝΟΙ',
		               'ΟΤΕΡΟΥΣ', 'ΕΣΤΑΤΟΥ', 'ΕΣΤΑΤΟΣ', 'ΟΥΣΤΕΡΑ', 'ΕΣΤΑΤΕΣ', 'ΥΤΑΤΟΥΣ', 'ΕΣΤΕΡΟΥ', 'ΕΣΤΕΡΟΣ', 'ΑΙΤΕΡΟΣ', 'ΑΙΤΕΡΟΥ', 'ΕΣΤΑΤΟΙ',
		               'ΑΙΤΕΡΟΙ', 'ΑΙΤΕΡΩΝ', 'ΑΙΤΕΡΗΣ', 'ΑΙΤΕΡΑΣ', 'ΟΥΜΕΝΟΥ', 'ΟΥΜΕΝΟΣ', 'ΟΥΜΕΝΗΣ', 'ΟΥΜΕΝΩΝ', 'ΟΥΜΕΝΕΣ', 'ΟΜΕΝΟΥΣ', 'ΕΣΤΑΤΩΝ',
		               'ΕΣΤΕΡΟΝ', 'ΗΜΕΝΟΥΣ', 'ΟΥΣΤΑΤΗ', 'ΟΥΣΤΑΤΑ', 'ΕΣΤΕΡΟΝ', 'ΟΥΣΤΑΤΟ', 'ΩΤΕΡΟΥΣ', 'ΩΤΑΤΟΥΣ']
        exbig_suffix = ['ΟΥΣΤΕΡΟΥ', 'ΟΥΣΤΕΡΟΣ', 'ΕΣΤΕΡΟΥΣ', 'ΟΥΣΤΕΡΗΣ', 'ΕΣΤΑΤΟΥΣ', 'ΟΥΣΤΕΡΩΝ', 'ΟΥΣΤΑΤΕΣ', 'ΟΥΣΤΕΡΕΣ', 'ΟΥΣΤΕΡΟΙ', 'ΑΙΤΕΡΟΥΣ',
                        'ΟΥΣΤΑΤΟΣ', 'ΟΥΣΤΑΤΟΥ', 'ΟΥΣΤΑΤΗΣ', 'ΟΥΣΤΑΤΩΝ']

        for suffix in ['ΟΥΣΤΕΡΟΥΣ', 'ΟΥΣΤΑΤΟΥΣ'] :
            if ends_with(word, suffix) :
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 1 :
					return word[:len(word) - len(suffix)]
        for suffix in exbig_suffix :
            if ends_with(word, suffix) :
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 1 :
					return word[:len(word) - len(suffix)]
        for suffix in big_suffix :
            if ends_with(word, suffix) :
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 1 :
					return word[:len(word) - len(suffix)]
        for suffix in mdbig_suffix :
            if ends_with(word, suffix) :
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 1 :
					return word[:len(word) - len(suffix)]
        for suffix in med_suffix : 
			if ends_with(word, suffix) :
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 1 :
					return word[:len(word) - len(suffix)]
        for suffix in ['ΜΕΝΟ', 'ΜΕΝΗ', 'ΜΕΝΑ', 'ΕΙΕΣ', 'ΕΙΩΝ'] :
			if ends_with(word, suffix) :
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 1 :
					return word[:len(word) - len(suffix)]
        for suffix in  ['ΟΥΣ', 'ΕΩΣ', 'ΕΟΣ', 'ΩΣΑ', 'ΟΥΝ', 'ΕΙΣ', 'ΟΥΣ', 'ΕΩΝ'] :
			if ends_with(word, suffix) :
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 1 :
					return word[:len(word) - len(suffix)]
        for suffix in ['ΙΣ', 'ΟΣ', 'ΥΣ', 'ΟΥ', 'ΑΣ', 'ΗΣ', 'ΟΣ', 'ΕΣ', 'ΕΑ', 'ΩΝ', 'ΤΙ', 'ΕΙ', 'ΟΝ', 'ΑΝ', 'ΕΝ', 'ΙΝ', 'ΟΙ'] :
			if ends_with(word, suffix) :
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 1 :
					return word[:len(word) - len(suffix)]
        for suffix in ['Η', 'Α', 'Ο',  'Ι', 'Υ', 'Ε'] :
			if ends_with(word, suffix):
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]


#######rule-set 7 --- Singular Noun
    elif typePOS in ['NNM', 'NNF', 'NNN', 'NNPM', 'NNPF', 'NNPN'] :
        
        if typePOS in ['NNF', 'NNPF'] :
			if word.decode('utf-8')[-3:] in ['ΕΑΣ', 'ΙΑΣ'] :
				if len(word.decode('utf-8')[:-2]) > 1 :
					return word.decode('utf-8')[:-2].encode('utf-8')
        for suffix in ['ΟΥΣ', 'ΕΩΣ', 'ΕΟΣ', 'ΟΥΝ', 'ΕΙΣ'] :
			if (typePOS in ['NNF', 'NNPF']) and (suffix=='ΤΗΣ'): # if word is female and suffix is -ΤΗΣ
				continue
			if ends_with(word, suffix) :
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]
        for suffix in ['ΥΣ', 'ΩΣ', 'ΟΥ', 'ΑΣ', 'ΗΣ', 'ΟΣ', 'ΕΣ', 'ΩΝ', 'ΕΙ', 'ΟΝ', 'ΑΝ', 'ΕΝ', 'ΙΝ', 'ΟΙ', 'ΙΣ']:
			if ends_with(word, suffix):
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]
        for suffix in ['Η', 'Α', 'Ω', 'Ο',  'Ι', 'Ε'] :
			if ends_with(word, suffix):
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]

######rule-set 8 --- Plural Noun
    elif typePOS in ['NNSM', 'NNSF', 'NNSN', 'NNPSM', 'NNPSF', 'NNPSN'] :
        for suffix in ['ΕΙΣΕΣ', 'ΕΙΣΩΝ', 'ΙΑΔΕΣ', 'ΙΑΔΩΝ', 'ΟΥΔΕΣ', 'ΟΥΔΩΝ', 'ΙΜΑΤΑ'] :
			if ends_with(word, suffix) :
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]
        for suffix in ['ΟΥΣ', 'ΕΙΣ', 'ΕΩΝ'] :
			if ends_with(word, suffix):
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]	
        for suffix in ['ΟΙ', 'ΩΝ', 'ΕΣ', 'ΕΑ'] :
			if ends_with(word, suffix):
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]
        for suffix in ['Α', 'Η'] :
			if ends_with(word, suffix):
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]

#######rule-set 9 --- Adverb
    elif typePOS=='RB':
		for suffix in ['ΟΥΣΤΑΤΑ', 'ΑΙΤΕΡΑ', 'ΑΙΤΕΡΩΣ', 'ΟΤΑΤΑ', 'ΕΣΤΑΤΑ', 'ΥΤΑΤΑ'] :
			if ends_with(word, suffix):
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]
		for suffix in ['ΟΤΕΡΟ', 'ΟΤΕΡΑ', 'ΕΣΤΕΡΑ', 'ΑΙΤΕΡΑ', 'ΥΤΕΡΑ', 'ΑΣΙΑ', 'ΜΕΝΑ', 'ΕΩΣ', 'ΤΑΤΑ'] :
			if ends_with(word, suffix):
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]      
		for suffix in ['ΩΣ', 'ΟΥ'] :
			if ends_with(word, suffix):
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]
		for suffix in ['Α', 'Υ', 'Ο'] :
			if ends_with(word, suffix):
				if (len(word.decode('utf-8')) - len(suffix.decode('utf-8'))) > 0 :
					return word[:len(word) - len(suffix)]

    return word


